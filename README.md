# WindowsDropper

Windows dropper + Reverse shell payload

[](https://gitlab.com/BBanshy/windowsdropper/-/raw/master/Dropper+PayloadArchi.png)
### 

First step: 

1. Open any Bat to Exe converter and make an .exe file with the Dropper.bat file


Second step: 

2. Launch any kind of Webserver to upload the payload.bat file

	Exemple: 'python -m SimpleHttpServer'

Third step:

3. Launch the .exe on the victim VM (Win10 last build) and for the reboot. 
Then, victim's computer will create a /temp/ directory and download and execute  netcat and the payload, who do a ReverseShell to the Attacker via netcat ! 

